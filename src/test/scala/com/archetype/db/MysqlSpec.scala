package com.archetype
package db

import org.scalatest.{BeforeAndAfterAll, Suite}
import slick.jdbc.MySQLProfile.api._

trait MysqlSpec extends Suite with BeforeAndAfterAll {
  val db: Database = Database.forConfig("mysql")

  override def beforeAll: Unit = {}

  override def afterAll: Unit = {
    db.close()
  }
}
