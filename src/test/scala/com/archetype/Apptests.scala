package com.archetype

import org.scalatest.tagobjects.Slow
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable.{Stack => MutableStack}

class Apptests extends FlatSpec with Matchers {
  "A Stack" should "pop values in a FIFO order" in {
    val theStack = new MutableStack[Int]
    theStack.push(5)
    theStack.push(6)
    theStack.pop() shouldBe (6)
    theStack.pop() shouldBe (5)
  }

  it should "Throw exception if you're popping bottles with your broke ass" taggedAs (Slow) in {
    val theStack = new MutableStack[Int]

    val x = 3
    assert(x == 3)

    a[NoSuchElementException] should be thrownBy {
      theStack.pop()
    }
  }
}
