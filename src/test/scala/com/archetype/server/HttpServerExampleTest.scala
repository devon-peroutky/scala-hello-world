package com.archetype
package server

import io.finch._
import org.scalatest.FunSuite

class HttpServerExampleTest extends FunSuite {
  test("healthcheck") {
    assert(
      HttpServerExample.healthcheck(Input.get("/")).awaitValueUnsafe() == Some(
        "OK"))
  }
}
