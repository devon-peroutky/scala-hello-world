package com.archetype
package cache

import cats.data.EitherT
import cats.implicits._
import com.github.blemale.scaffeine.AsyncLoadingCache
import org.scalatest.{AsyncFlatSpec, Matchers}

import scala.concurrent.Future

class TestCacheFactory extends AsyncFlatSpec with Matchers {

  "CacheBuilder" should "build a simple cache" in {
    val cache = CacheFactory.buildCache[Int, String]()

    cache.put(1, "foo")

    cache.getIfPresent(1) should be(Some("foo"))
    cache.getIfPresent(2) should be(None)
  }

  "AsyncCacheBuilder" should "Load an EitherT asynchronously" in {
    val loader: Int => FutureResponse[String] = { _ =>
      EitherT(Future {
        Either.right("Result")
      })
    }
    val loadingCache: AsyncLoadingCache[Int, String] =
      CacheFactory.buildAsyncLoadingCache(loader = loader)

    loadingCache.get(1).map { value =>
      value shouldEqual "Result"
    }
  }
}
