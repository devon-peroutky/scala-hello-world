package com.archetype

import scala.concurrent.Future
import io.finch._
import scala.concurrent.Future
import cats.implicits._
import io.circe.Encoder
import scala.concurrent.ExecutionContext.Implicits.global
import io.finch.Output
import cats.syntax._

package object server {
  implicit class ClassToResponse[D: Encoder](entity: FutureResponse[D]) {
    def toHttpResponse: Future[Output[D]] = {
      entity.value.map {
        case Right(x)  => Ok(x)
        case Left(err) => err.toHttpResponse
      }
    }
  }
}
