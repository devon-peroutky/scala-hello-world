package com.archetype
package server

import cats.effect.IO
import com.twitter.finagle.{Http, Service}
import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.util.Await
import io.finch._
import io.finch.circe._
import io.circe.generic.auto._

import com.archetype.Errors.{HttpServiceError, GenericError, SampleError}
import scala.concurrent.ExecutionContext.Implicits.global
import cats.implicits._
import io.circe._
import cats.data.EitherT

import scala.concurrent.Future
import shapeless.HNil

object HttpServerExample extends App with Endpoint.Module[IO] {
  def mockFutureResponse: FutureResponse[Int] = {
    EitherT(Future {
      Either.right[HttpServiceError, Int](6)
      Either.left[HttpServiceError, Int](SampleError("BROO"))
    })
  }

  def healthcheck: Endpoint[IO, String] = get(pathEmpty) {
    Ok("OK")
  }

  val ageParam = param[Int]("age").shouldNot("be less than 18") { _ < 18 }
  def time: Endpoint[IO, Time] =
    post(
      "time" :: path[String] :: "now" :: jsonBody[Locale] :: ageParam :: headerOption(
        "auth")) {
      (timeZone: String, l: Locale, age: Int, authHeader: Option[String]) =>
        Ok(Time(l, System.currentTimeMillis().toString))
    }

  val redirect: Endpoint[IO, String] = get("redirect" :: "from") {
    Output
      .empty[String](Status.MovedPermanently)
      .withHeader("Location" -> "/redirect/to")
  }

  def service: Service[Request, Response] =
    Bootstrap
      .serve[Text.Plain](healthcheck)
      .serve[Text.Plain](redirect)
      .serve[Application.Json](time)
      .toService

  println("Ready")
  Await.ready(Http.server.serve(":8081", service))
}
