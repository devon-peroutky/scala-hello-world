package com

import scala.concurrent.Future
import cats.data.EitherT
import io.finch._

import scala.concurrent.Future
import cats.implicits._
import io.circe.Encoder

import scala.concurrent.ExecutionContext.Implicits.global
import io.finch.Output
import cats.syntax._
import cats.implicits._
import com.archetype.Errors.{HttpServiceError, GenericError}

package object archetype {
  case class Locale(language: String, country: String)
  case class Time(locale: Locale, time: String)

  type FutureResponse[D] = EitherT[Future, HttpServiceError, D]

  implicit class FutureToEitherT[D](x: Future[D]) {
    def toEitherT: FutureResponse[D] = {
      EitherT {
        x.map(res => Either.right[HttpServiceError, D](res)).recover {
          case e: HttpServiceError => Either.left[HttpServiceError, D](e)
          case other: Throwable =>
            Either.left[HttpServiceError, D](GenericError(other.getMessage))
        }
      }
    }
  }

  implicit class ClassToResponse[D: Encoder](entity: FutureResponse[D]) {
    def toHttpResponse: Future[Output[D]] = {
      entity.value.map {
        case Right(x)  => Ok(x)
        case Left(err) => err.toHttpResponse
      }
    }
  }
}
