package com.archetype

import cats.effect.IO
import io.finch._
import com.twitter.finagle.Http
import com.twitter.util.Await
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import cats.implicits._
import com.archetype.server.HttpServerExample

/**
  * 1. Define the table schemas
  * 2. Create the tables in MySQL (Deleting the Users/Tutorials table is more work)
  * 3. sbt genTables
  * 4. Make sure Main can run as well as a Test
  * 5. Code away.
  */
object Main extends App with Endpoint.Module[IO] {
  val db = Database.forConfig("mysql")

  println("Server ready on 8081")
  Await.ready(Http.server.serve(":8081", HttpServerExample.service))
}
