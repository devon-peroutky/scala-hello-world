package com.archetype

import io.circe._
import io.circe.parser._

import scala.io.Source

package object fileio {

  def readJsonFile[D](fileName: String)(
      implicit decoder: Decoder[D]): Either[ParsingFailure, D] = {
    val fileReader = Source.fromResource(fileName)
    parse(fileReader.getLines().mkString).flatMap(_.as[D]) match {
      case Left(failure) =>
        Left(ParsingFailure(s"Failed to parse: $fileName", failure))
      case Right(objs) => Right(objs)
    }
  }

  def parseJson[D](fileName: String)(implicit decoder: Decoder[D]): D = {
    readJsonFile[D](fileName) match {
      case Left(err)    => throw err
      case Right(value) => value
    }
  }
}
