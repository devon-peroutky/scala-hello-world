package com.archetype
package fileio

import com.archetype.client.PostBody
import io.circe.generic.auto._

import scala.io.Source
import scala.util.{Failure, Success, Try}

object IOExample extends App {

  /** Read File line by line **/
  val fileReader = Source.fromResource("dummydata.json")
  for (line <- fileReader.getLines()) {
    println(line)
  }

  /** Read entire File into a string **/
  Try(fileReader.getLines().toList) match {
    case Success(lines) => lines foreach println
    case Failure(err)   => println(err)
  }

  val posts = parseJson[List[PostBody]]("dummydata.json")
  println(posts)

  /** Normally you would wrap the above in a try-finally and close the file (so it doesn't stay open
    * for the life of the JVM), but given we're scripting, it's fine to omit.
    */
  fileReader.close()
}
