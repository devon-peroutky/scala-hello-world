package com.archetype

package object db {
  case class NewUser(firstName: String, lastName: String)
  case class NewTutorial(authorId: Long,
                         title: String,
                         dateAuthored: Option[String] = None)
}
