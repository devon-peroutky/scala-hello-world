package com.archetype

import io.finch.Output
import io.finch._

object Errors {

  trait ServiceError extends Throwable

  trait HttpServiceError extends ServiceError {
    def toHttpResponse: Output[Nothing]
  }

  case class SampleError(message: String) extends HttpServiceError {
    override def toHttpResponse: Output[Nothing] =
      NotImplemented(new Exception(message))
  }

  case class GenericError(message: String) extends HttpServiceError {
    override def toHttpResponse: Output[Nothing] =
      InternalServerError(new Exception(message))
  }
}
