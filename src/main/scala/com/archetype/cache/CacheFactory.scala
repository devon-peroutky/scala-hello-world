package com.archetype
package cache

import com.github.blemale.scaffeine.{AsyncLoadingCache, Cache, Scaffeine}
import cats.implicits._
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.duration._

object CacheFactory {

  /**
    * If the key does not exists, or has expired, the cache will load it using the passed Loader.
    * If the loader encounters an error, than the exception throw will be swallowed, and the key will not be overwritten.
    * @param loader
    * @param refreshPeriod
    * @param expirationPeriod
    * @param maxSize
    * @tparam K
    * @tparam V
    * @return
    */
  def buildAsyncLoadingCache[K, V](
      loader: K => FutureResponse[V],
      refreshPeriod: FiniteDuration = 1.minute,
      expirationPeriod: FiniteDuration = 1.hour,
      maxSize: Int = 500): AsyncLoadingCache[K, V] =
    Scaffeine()
      .expireAfterWrite(expirationPeriod)
      .refreshAfterWrite(refreshPeriod)
      .maximumSize(maxSize)
      .buildAsyncFuture { key =>
        loader(key).value
          .map(either => either.fold(e => throw e, r => r))
      }

  def buildCache[K, V](expirationPeriod: FiniteDuration = 1.hour,
                       maxSize: Int = 500): Cache[K, V] =
    Scaffeine()
      .expireAfterWrite(expirationPeriod)
      .maximumSize(maxSize)
      .build[K, V]()
}
