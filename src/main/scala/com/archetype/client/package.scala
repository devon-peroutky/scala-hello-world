package com.archetype

import cats.data.EitherT
import io.circe._
import io.circe.parser._
import io.circe.syntax._
import io.circe.{Decoder, Encoder}
import scalaj.http.{HttpRequest, HttpResponse}

import scala.concurrent.{Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global

package object client {

  type FutureHttpResponse[D] = EitherT[Future, Exception, D]

  case class PostBody(title: String, body: String, userId: Int)

  case class HttpErrorResponse(request: HttpRequest,
                               response: HttpResponse[String])
      extends Exception(
        s"Error making http request: $request with error $response") {}

  case class ResponseDecodingError(res: String, e: io.circe.Error)
      extends Exception(s"""Encountered error decoding $res with error $e""")

  implicit class RequestToJson(req: HttpRequest) {

    def postJson[D](a: D)(implicit encoder: Encoder[D]): HttpRequest = {
      req.postData(a.asJson.noSpaces).header("content-type", "application/json")
    }

    def toEither[D: Decoder]: Either[Exception, D] = {
      val resp = req.asString
      if (resp.isSuccess) {
        decode[D](resp.body) match {
          case Right(value) => Right(value)
          case Left(error)  => Left(ResponseDecodingError(resp.body, error))
        }
      } else {
        Left(HttpErrorResponse(req, resp))
      }
    }

    def toEntity[D: Decoder]: D = {
      req.toEither[D] match {
        case Right(value) => value
        case Left(err)    => throw err
      }
    }

    def asyncExec[D: Decoder]: FutureHttpResponse[D] = {
      EitherT(Future {
        blocking {
          req.toEither
        }
      })
    }
  }
}
