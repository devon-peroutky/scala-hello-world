package com.archetype
package client

import scalaj.http._
import io.circe.generic.auto._

import scala.concurrent.Await
import scala.concurrent.duration._

object HttpClientExample extends App {
  val baseUrl = "https://jsonplaceholder.typicode.com"

  val getRequest: HttpRequest = Http(s"$baseUrl/comments").param("postId", "1")
  val postRequest: HttpRequest =
    Http(s"$baseUrl/comments")
      .param("postId", "1")
      .postJson(PostBody("The Title", "The Body", 1))

  val getResponse: HttpResponse[String] = getRequest.asString
  val entityEither: Either[Exception, PostBody] = postRequest.toEither[PostBody]
  val asyncResp: FutureHttpResponse[PostBody] = postRequest.asyncExec[PostBody]

  Await.ready(asyncResp.value, 10.seconds)
  println(asyncResp.value)

}
