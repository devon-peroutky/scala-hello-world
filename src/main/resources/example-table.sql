/**
1. docker exec -it {name} bash
2. su mysql; mysql -uroot -p
3. SHOW DATABASES;
4. CREATE DATABASE {name}
4. USE DATABASE {name}
 */
CREATE table Users(
   user_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
   first_name VARCHAR(100) NOT NULL,
   last_name VARCHAR(40) NOT NULL
);

CREATE table Tutorials(
   tutorial_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
   tutorial_title VARCHAR(100) NOT NULL,
   tutorial_author_id BIGINT NOT NULL,
   submission_date LONG,
   FOREIGN KEY (tutorial_author_id) REFERENCES Users(user_id)
);