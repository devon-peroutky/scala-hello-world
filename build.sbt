name := "scala-archetype"
version := "0.0.1"
scalaVersion := "2.12.7"

val finchVersion = "0.31.0"
val circeVersion = "0.12.3"
val scalatestVersion = "3.0.5"
val slickVersion = "3.3.0"

lazy val root = (project in file("."))
  .settings(
    organization := "com",
    name := name.value,
    version := version.value,
    scalaVersion := scalaVersion.value,
    libraryDependencies ++= Seq(
      "com.github.finagle" %% "finchx-core"  % finchVersion,
      "com.github.finagle" %% "finchx-circe"  % finchVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "org.scalatest"      %% "scalatest"    % scalatestVersion % "test",
      "org.scalaj" %% "scalaj-http" % "2.4.2",
      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,
      "com.typesafe.slick" %% "slick" % slickVersion,
      "com.typesafe.slick" %% "slick-codegen" % slickVersion,
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "org.slf4j" % "slf4j-nop" % "1.6.4",
      "mysql" % "mysql-connector-java" % "8.0.15",
      "com.github.blemale" %% "scaffeine" % "3.1.0" % "compile",
      "org.scalamock" %% "scalamock" % "4.4.0" % Test,
      "com.github.tminglei" %% "slick-pg" % "0.16.2",
      "com.github.tminglei" %% "slick-pg_circe-json" % "0.16.2"
    ),
    slick := slickCodeGenTask.value, // register manual sbt command
//    sourceGenerators in Compile += slickCodeGenTask // register automatic code generation on every compile, remove for only manual use
  )


lazy val slick = TaskKey[Seq[File]]("gen-tables")
lazy val slickCodeGenTask = Def task {
  val dir = sourceManaged.value
  val cp = (dependencyClasspath in Compile).value
  val r = (runner in Compile).value
  val s = streams.value
  val base = baseDirectory.value
  println(s"Source Dir: ${sourceDirectory.value}")

  val outputDir = (dir / "slick").getPath
  val jdbcDriver = "com.mysql.cj.jdbc.Driver"
  val slickDriver = "slick.jdbc.MySQLProfile"
  val url = "jdbc:mysql://localhost:3306/interview-prep"
  val pkg = "com.archetype.db.generated"
//  r.run("slick.codegen.SourceCodeGenerator", cp.files, Array(slickDriver, jdbcDriver, url, outputDir, pkg, "root", "interview-prep"), s.log)
  val fname = s"$base/src/main/scala"
  r.run("slick.codegen.SourceCodeGenerator", cp.files, Array(slickDriver, jdbcDriver, url, fname, pkg, "interview", "interview"), s.log)
  Seq(file(fname))
}
slick := slickCodeGenTask.value // register manual sbt command "genTables"
